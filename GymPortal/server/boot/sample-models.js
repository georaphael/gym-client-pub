// Copyright IBM Corp. 2015,2016. All Rights Reserved.
// Node module: loopback-example-access-control
// This file is licensed under the Artistic License 2.0.
// License text available at https://opensource.org/licenses/Artistic-2.0

var userdetails;

module.exports = function (app) {
  var User = app.models.userinfo;
  var Gymownerrole = app.models.gymownerrole;
  var Role = app.models.Role;
  var RoleMapping = app.models.RoleMapping;
  // var Team = app.models.Team;

  User.find({ where: { email: { "neq": 'admin@gmail.com' } } }).
    then(client => {
      User.create([
        { username: 'admin', email: 'admin@gmail.com', password: 'admin' }

      ], function (err, users) {
      });
    }).catch(err => {
      cb(err);
      return;
    });


  User.find({ where: { email: 'admin@gmail.com' } }).
    then(users => {
      userdetails = users;

      console.log('log is ' + users[0].id);


      Role.create({
        name: 'admin'
      }, function (err, role) {
        if (err) throw err;

        console.log('Created role:');

        //make bob an admin
        role.principals.create({
          principalType: RoleMapping.USER,
          principalId: userdetails[0].id
        }, function (err, principal) {
          if (err) throw err;

          console.log('Created principal:', principal);
        });
      });


      Role.create({
        name: 'gymowner'
      }, function (err, role) {
        if (err) throw err;

        console.log('Created role for manager:');


        // User.find({ where: { principalId: "5c874538346329125e59e85e" } })
        // .then(clientInstance => {

        // RoleMapping.find({ where: { principalId: "5c874538346329125e59e85e" } })
        // .then(clientInstance => {
        //   clientInstance.forEach(function (clientInstances) {
        //   console.log(JSON.stringify(clientInstances));
        //   });
        // });

        //make bob an admin

//        app.models.Gymownerrole.find({ where: { roleId: 1 } }).
	         app.models.Gymownerrole.find().	
          then(rolemapping => {
            rolemapping.forEach(function (rolemappings) {

              role.principals.create({
                principalType: RoleMapping.USER,
                principalId: rolemappings.principalId
              }, function (err, principal) {
                if (err) throw err;

                console.log('Created principal:1', principal);
                //return;
              });
            });
		//return ;
          });
        });




      }).catch(err => {
        cb(err);
        return;
      });



      // User.create([
      //   { username: 'admin', email: 'admin@gmail.com', password: 'admin' }


      // ], function (err, users) {
      //   if (err) throw err;
      //   console.log("hallo");







      //  });

      // User.create([
      //   {username: 'John', email: 'john@doe.com', password: 'opensesame'},
      //   {username: 'Jane', email: 'jane@doe.com', password: 'opensesame'},
      //   {username: 'Bob', email: 'bob@projects.com', password: 'opensesame'}
      // ], function(err, users) {
      //   if (err) throw err;

      //   console.log('Created users:', users);

      //   // create project 1 and make john the owner
      //   users[0].projects.create({
      //     name: 'project1',
      //     balance: 100
      //   }, function(err, project) {
      //     if (err) throw err;

      //     console.log('Created project:', project);

      //     // add team members
      //     Team.create([
      //       {ownerId: project.ownerId, memberId: users[0].id},
      //       {ownerId: project.ownerId, memberId: users[1].id}
      //     ], function(err, team) {
      //       if (err) throw err;

      //       console.log('Created team:', team);
      //     });
      //   });

      //   //create project 2 and make jane the owner
      //   users[1].projects.create({
      //     name: 'project2',
      //     balance: 100
      //   }, function(err, project) {
      //     if (err) throw err;

      //     console.log('Created project:', project);

      //     //add team members
      //     Team.create({
      //       ownerId: project.ownerId,
      //       memberId: users[1].id
      //     }, function(err, team) {
      //       if (err) throw err;

      //       console.log('Created team:', team);
      //     });
      //   });

      //   //create the admin role
      //   Role.create({
      //     name: 'admin'
      //   }, function(err, role) {
      //     if (err) throw err;

      //     console.log('Created role:', role);

      //     //make bob an admin
      //     role.principals.create({
      //       principalType: RoleMapping.USER,
      //       principalId: users[2].id
      //     }, function(err, principal) {
      //       if (err) throw err;

      //       console.log('Created principal:', principal);
      //     });
      //   });
      // });
    };
