import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { RegisterService } from '../register.service';
import { Response } from '@angular/http/src/static_response';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent  {

  constructor(private router: Router,
    private route: ActivatedRoute,
    private loginService: RegisterService
  ) {

  }

  ngOnInit() {
  }
  onFormSubmit(data) {
    console.log(data.email);
    this.loginService.registerUser(data).subscribe(result => {
      //  console.log('id is '+result.id)
      //  sessionStorage.setItem('userId',result.userId);
      //  sessionStorage.setItem('token',result.id);
      // if (result.id != '') {
      //   sessionStorage.setItem('userName',data.email)
      //   this.router.navigateByUrl('/home');
      // }
      // else{
      //   this.router.navigateByUrl('/login');
      // }
    })
  }

}
