import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GymownerhomeComponent } from './gymownerhome.component';

describe('GymownerhomeComponent', () => {
  let component: GymownerhomeComponent;
  let fixture: ComponentFixture<GymownerhomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GymownerhomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GymownerhomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
