import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from './login.service';
import { Response } from '@angular/http/src/static_response';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'login',
  templateUrl: './login.component.html'
})
export class LoginComponent {

  constructor(private router: Router,
    private route: ActivatedRoute,
    private loginService: LoginService
  ) {

  }


  //   ngOnInit() {

  //     this.getProducts();
  //   }




  onFormSubmit(data) {
    console.log(data.email);
    this.loginService.loginUser(data).subscribe(result => {
       console.log('id is '+result.id)
       sessionStorage.setItem('userId',result.userId);
       sessionStorage.setItem('token',result.id);
      if (result.id != '') {

        this.loginService.findRole(result.userId).subscribe(role => {
          console.log('with in controller'+JSON.stringify(role.det))
          console.log('single'+role.det[0].roleId)


          if(role.det[0].roleId == 2)
          this.router.navigateByUrl('/gymownerhome');

          if(role.det[0].roleId == 1 ||  role.det[0].roleId == "5c8135749624af4f09d548aa")
          this.router.navigateByUrl('/adminhome');

        });

        sessionStorage.setItem('userName',data.email)
        this.router.navigateByUrl('/home');
      }
      else{
        this.router.navigateByUrl('/login');
      }
    })
  }
}



