import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

@Injectable()
export class LoginService {

    constructor(private http: Http) { }


    loginUser(user) {
        console.log('request '+JSON.stringify(user));
        return this.http.post('http://localhost:4000/api/userinfos/login', user)
            .map((response: Response) => {
                var myJSON1 = JSON.stringify(response);
                console.log(myJSON1);

                var par = response.text();
                if (par != '') {
                    var obj = JSON.parse(par);
                    return obj;
                } else {
                    return -1;

                }
            })
    }


    
    
        

    findRole(id) {

        var token = sessionStorage.getItem('token');
        var data={
            id
        };
        data.id=id;
        //console.log('request '+JSON.stringify(user));
        var url='http://localhost:4000/api/userinfos/findRole?id='+id+'&access_token='+token;
       // var url='http://localhost:4000/api/userinfos/findRole?access_token='+token;
        console.log('url is'+url);
        return this.http.get(url)
            .map((response: Response) => {

                var myJSON1 = JSON.stringify(response);
                console.log('Role is '+myJSON1);

                var par = response.text();
                if (par != '') {
                    var obj = JSON.parse(par);
                    return obj;
                } else {
                    return -1;

                }
            });
        }               
}   