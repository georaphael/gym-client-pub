import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { RegisterService } from '../register.service';
import { Response } from '@angular/http/src/static_response';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-registergymowner',
  templateUrl: './registergymowner.component.html',
  styleUrls: ['./registergymowner.component.css']
})
export class RegistergymownerComponent  {

  constructor(private router: Router,
    private route: ActivatedRoute,
    private loginService: RegisterService
  ) {

  }


  ngOnInit() {
  }

  onFormSubmit(data) {
    console.log(data.email);
    this.loginService.registerGymowner(data).subscribe(result => {
      alert('registered successfully')
      this.router.navigateByUrl('/login');

      //  console.log('id is '+result.id)
      //  sessionStorage.setItem('userId',result.userId);
      //  sessionStorage.setItem('token',result.id);
      // if (result.id != '') {
      //   sessionStorage.setItem('userName',data.email)
      //   this.router.navigateByUrl('/home');
      // }
      // else{
      //   this.router.navigateByUrl('/login');
      // }
    })
  }

}
