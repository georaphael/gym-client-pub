import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistergymownerComponent } from './registergymowner.component';

describe('RegistergymownerComponent', () => {
  let component: RegistergymownerComponent;
  let fixture: ComponentFixture<RegistergymownerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistergymownerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistergymownerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
