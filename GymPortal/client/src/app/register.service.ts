
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';


@Injectable({
    providedIn: 'root'
})
export class RegisterService {

    constructor(private http: Http) { }

    registerUser(user) {
        console.log('request ' + JSON.stringify(user));
        var token = sessionStorage.getItem('token');

        return this.http.post('http://localhost:4000/api/userinfos/register?access_token=' + token, user)
            .map((response: Response) => {
                console.log('registered Gymowner');
                var myJSON1 = JSON.stringify(response);
                console.log(myJSON1);
              //   console.log(JSON.stringify(result));  

                // var par = response.text();
                // if (par != '') {
                //     var obj = JSON.parse(par);
                //     return obj;
                // } else {
                //     return -1;

                // }
            })
    }

    registerGymowner(user) {
        console.log('request ' + JSON.stringify(user));
        var token = sessionStorage.getItem('token');

        return this.http.post('http://localhost:4000/api/userinfos/registerGymOwner?access_token=' + token, user)
            .map((response: Response) => {
                var myJSON1 = JSON.stringify(response);
                console.log('status is'+myJSON1);
                
                return;

                // var par = response.text();
                // if (par != '') {
                //     var obj = JSON.parse(par);
                //     return obj;
                // } else {
                //     return -1;

                // }
            })
    }
}
