import { TestBed, inject } from '@angular/core/testing';

import { GymownerhomeService } from './gymownerhome.service';

describe('GymownerhomeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GymownerhomeService]
    });
  });

  it('should be created', inject([GymownerhomeService], (service: GymownerhomeService) => {
    expect(service).toBeTruthy();
  }));
});
