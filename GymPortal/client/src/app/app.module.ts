import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination'; // <-- import the module
import { AppComponent } from './app.component';
import { OrderModule } from 'ngx-order-pipe';
import { Ng2SearchPipeModule } from 'ng2-search-filter'; //importing the module
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
//import { AdminComponent } from './admin/admin.component';
import { LoginComponent } from './login/login.component';
//import { AdminService } from './admin/admin.service';
import { LoginService } from './login/login.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './home/home.component';
//import { EditProductComponent } from './edit-product/edit-product.component';
import { RegisterComponent } from './register/register.component';
import { GymownerhomeComponent } from './gymownerhome/gymownerhome.component';
import { AdminhomeComponent } from './adminhome/adminhome.component';
import { RegistergymownerComponent } from './registergymowner/registergymowner.component';
@NgModule({
  declarations: [
    AppComponent,
  //  AdminComponent,
    LoginComponent,
    HomeComponent,
    //EditProductComponent,
    RegisterComponent,
    GymownerhomeComponent,
    AdminhomeComponent,
    RegistergymownerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgbModule,
    HttpModule,
    NgxPaginationModule,
    OrderModule,
    Ng2SearchPipeModule,
    HttpClientModule,
    RouterModule.forRoot([
      // {
      //   path: 'admin',
      //   component: AdminComponent
      // },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: '',
        component: LoginComponent
      },
      {
        path: 'register',
        component: RegisterComponent
      },
      {
        path: 'registergymowner',
        component: RegistergymownerComponent
      },
      {
        path: 'adminhome',
        component: AdminhomeComponent
      },
      {
        path: 'gymownerhome',
        component: GymownerhomeComponent
       }
       //,
      // {
      //   path: 'edit/:id',
      //   component: EditProductComponent
      // }
    ])
  ],
  providers: [LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }

