'use strict';

module.exports = function(Customerdetails) {

/**
 * 
 * @param {number} customerdetailsid 
 * @param {Function(Error, object)} callback
 */

Customerdetails.getEmergencyNumber = function(customerdetailsid, callback) {
    Customerdetails.findOne({where: {customer_details_id : customerdetailsid}}, function(err, customerdetails){
        console.log(customerdetails.emergency_contact);
        callback(null, customerdetails.emergency_contact);
    })
};

Customerdetails.getAccountDetails = function(customerid, callback) {
    Customerdetails.findOne({where: {customer_details_id : customerid}}, function(err, accountDetails){
        console.log(accountDetails);
        callback(null, accountDetails);
    })
};

Customerdetails.getProfileDetails = function(custid, callback) {
    Customerdetails.findOne({where: {customer_details_id : custid}}, function(err, profileDetails){
        console.log(profileDetails);
        callback(null, profileDetails);
    })
};
 
Customerdetails.remoteMethod(
    'getEmergencyNumber',{
      http: {
        path: '/getEmergencyNumber',
        verb: 'get'
      },
      accepts: [
        {arg: 'customerdetailsid', type: 'number', http: { source: 'query'}},
      ],
      returns: {
        arg: 'customerdetails',
        type: 'object',
        root: true
      }
    }
  ),

  Customerdetails.remoteMethod(
    'getAccountDetails',{
      http: {
        path: '/getAccountDetails',
        verb: 'get'
      },
      accepts: [
        {arg: 'customerid', type: 'number', http: { source: 'query'}},
      ],
      returns: {
        arg: 'accountDetails',
        type: 'object',
        root: true
      }
    }
  ),

  Customerdetails.remoteMethod(
    'getProfileDetails',{
      http: {
        path: '/getProfileDetails',
        verb: 'get'
      },
      accepts: [
        {arg: 'custid', type: 'number', http: { source: 'query'}},
      ],
      returns: {
        arg: 'profileDetails',
        type: 'object',
        root: true
      }
    }
  )


};
