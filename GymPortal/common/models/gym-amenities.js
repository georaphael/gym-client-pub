'use strict';

module.exports = function(Gymamenities) {
Gymamenities.validatesAbsenceOf("gym_amenities_id")

/**
 * To get Amenities Info
 * @param {number} gymamenitiesid
 * @param {Function(Error)} callback
 */

Gymamenities.getInfoAmenities = function(gymamenitiesid,callback) {
    Gymamenities.findOne({where: {gym_amenities_id : gymamenitiesid}}, function(err, gymamenities){
    callback(null, gymamenities.amenities);
    })
}; 

Gymamenities.remoteMethod(
    'getInfoAmenities',{
    http: {
        path: '/getInfoAmenities',
        verb: 'get'
    },

    accepts: [
        {arg: 'gymamenitiesid', type: 'number', http: { source: 'query'}},
    ],
    returns: { 
        arg: 'gymamenities',
        type: 'object',
        root: true
        }
    })
};
    




