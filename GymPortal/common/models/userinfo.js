'use strict';



var app = require('../../server/server');
var UserInfo = require('../../common/models/userinfo');
var Gymownerrole = require('../../common/models/gymownerrole');
var User = app.models.User;
var Role = app.models.Role;
var RoleMapping = app.models.RoleMapping;


module.exports = function (userinfo) {

  /**
   * registration for user
   * @param {null} body body
   * @param {string} body body
   * @param {null} req request
   * @param {Function(Error)} callback
   */


  var Reg;
  var clientInstance;
  var roleId;

  userinfo.registerGymOwner = function (request, callback) {

    Reg = request;
    console.log('request:' + JSON.stringify(request))



    app.models.Role.find({ where: { name: 'gymowner' } }).
      then(role => {

        roleId = role[0].id;
        console.log('role' + JSON.stringify(role[0]));

        userinfo.create(request).then(user => {
          
          
                    clientInstance = user;

                    console.log('reg'+Reg.role)
                    
          
                  //  if (Reg.role == 'gymowner') {
                      var roleMappingDetails = {};
                      roleMappingDetails.principalType = "ROLE";
                      roleMappingDetails.principalId = clientInstance.id;
                      roleMappingDetails.roleId = roleId;
                      console.log(roleId+'role' + JSON.stringify(clientInstance));

                       userinfo.app.models.Gymownerrole.create(roleMappingDetails)
			// then(role => {
			// app.start();
      //       cb(null, role);
      //   }).catch(err => {
      //       cb(err);
      //       return;
      //   });
			
                      //return RoleMapping.create(roleMappingDetails)
                      // return userinfo.app.models.RoleMapping.create(roleMappingDetails)
                       callback(null, "{'status':'success'}");
                  //  }
          
                    // userinfo.create(request)
                    //callback(null, "{'status':'success'}");
                  });

      }).catch(err => {
               cb(err,"{'status':'not'}");
               return;
           });
  };


  userinfo.remoteMethod('registerGymOwner', {
    accepts: [{ arg: 'request', type: 'userinfo', http: { source: 'body' } }],
    'description': 'registration for GymOwner',
    http: [
      {
        'path': '/registerGymOwner',
        'verb': 'post'
      }
    ]
  });



  userinfo.register = function (request, callback) {
    console.log('request:' + JSON.stringify(request))
    userinfo.create(request)
    callback(null, "{'status':'success'}");
  };


  userinfo.remoteMethod('register', {
    accepts: [{ arg: 'request', type: 'userinfo', http: { source: 'body' } }],
    'description': 'registration for user',
    http: [
      {
        'path': '/register',
        'verb': 'post'
      }
    ]
  });



  userinfo.findRole = function (id, cb) {
    
    app.models.RoleMapping.find({ where: { principalId: id } }).
        then(role => {
            cb(null, role);
        }).catch(err => {
            cb(err);
            return;
        });
};
userinfo.remoteMethod(
    'findRole',
    {
        http: { path: '/findRole', verb: 'get' },
        accepts: { arg: 'id', type: 'string' },
        returns: { arg: 'det', type: 'object' }
    }
);

}
